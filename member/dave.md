---
name: David Stein
image: https://s3.amazonaws.com/publicbucket2share/colonelpanix/colonelpanixmugshot.jpg
links:
    website: http://www.davidstein.us
---

Also known as the live coder Colonel Panix. Currently developing libraries and tools to make algorithmic performance more human.