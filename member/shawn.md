---
name: Shawn Lawson (Obi Wan Codenobi)
image: https://www.gravatar.com/avatar/40cb93a78c028fa5d397b20d81f7c6cf
links:
    website: http://www.shawnlawson.com
---

Luke, you’re going to find that many of the truths we cling to depend greatly on our own point of view.